#include <iostream>
#include <cmath>
#include "cylinder.h"

namespace geometry {

    Circle::Circle(const double radius)
        : radius(radius) { }

    double Circle::area() const {
        return M_PI * std::pow(radius, 2);
    }

    double Circle::circumference() const {
        return 2 * M_PI * radius;
    }

    Cylinder::Cylinder(const double radius, const double height)
        : height(height),
          circle(Circle(radius)) {}

    double Cylinder::area() const {
        return (2 * circle.area() + height * circle.circumference());
    }

    double Cylinder::volume() const {
        return circle.area() * height;
    }
}

int main() {
    geometry::Cylinder cylinder(5.0, 10.0);
    std::cout << "Cylinder area: " << cylinder.area() << "\n";
    std::cout << "Cylinder volume: " << cylinder.volume() << "\n";
    return 0;
}
