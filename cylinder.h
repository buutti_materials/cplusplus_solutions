#ifndef __CYLINDER_H_INCLUDED__
#define __CYLINDER_H_INCLUDED__

namespace geometry {

    class Circle {
    private:
        const double radius;
    public:
        Circle(const double radius);
        double area() const;
        double circumference() const;
    };

    class Cylinder {
    private:
        const double height;
        Circle circle;
    public:
        Cylinder(const double radius, const double height);
        double area() const;
        double volume() const;
    };
}

#endif
