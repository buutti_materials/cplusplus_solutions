#include <iostream>
#include <memory>
#include <cmath>
#include "cylinder2.h"

namespace geometry {

    Circle::Circle(const double radius)
        : radius(radius) { }

    double Circle::area() const {
        return M_PI * std::pow(radius, 2);
    }

    double Circle::circumference() const {
        return 2 * M_PI * radius;
    }

    Cylinder::Cylinder(const double radius, const double height)
        : height(height),
          circle(std::make_unique<Circle>(radius)) {}

    double Cylinder::area() const {
        return (2 * circle->area() + height * circle->circumference());
    }

    double Cylinder::volume() const {
        return circle->area() * height;
    }

    bool Cylinder::operator<(const Cylinder& other) const {
        return volume() < other.volume();
    }

    bool Cylinder::operator>(const Cylinder& other) const {
        return volume() > other.area();
    }

}

int main() {
    geometry::Cylinder smallCylinder(5.0, 10.0);
    geometry::Cylinder largeCylinder(5.0, 10.0);

    std::cout << "Small cylinder area: " << smallCylinder.area() << "\n";
    std::cout << "Small cylinder volume: " << smallCylinder.volume() << "\n";
    std::cout << "Large cylinder area: " << largeCylinder.area() << "\n";
    std::cout << "Large cylinder volume: " << largeCylinder.volume() << "\n";
    std::cout << "smallCylinder < largeCylinder: ";
    std::cout << (smallCylinder < largeCylinder) << "\n";
    std::cout << "smallCylinder > largeCylinder: ";
    std::cout << (smallCylinder > largeCylinder) << "\n";

    return 0;
}
