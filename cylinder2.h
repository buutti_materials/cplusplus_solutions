#ifndef __CYLINDER2_H_INCLUDED__
#define __CYLINDER2_H_INCLUDED__

#include <memory>

namespace geometry {

    class Circle {
    private:
        const double radius;
    public:
        Circle(const double radius);
        double area() const;
        double circumference() const;
    };

    class Cylinder {
    private:
        const double height;
        std::unique_ptr<Circle> circle;
    public:
        Cylinder(const double radius, const double height);
        double area() const;
        double volume() const;
        bool operator<(const Cylinder& other) const;
        bool operator>(const Cylinder& other) const;
    };
}

#endif
