#include <string>
#include <vector>
#include <stdexcept>
#include <iostream>
#include "queue.h"

template <typename T>
void Queue<T>::push(T elem) {
    elements.insert(elements.begin(), elem);
}

template <typename T>
T Queue<T>::pop() {
    if (elements.empty()) {
        throw std::out_of_range("Queue<>::pop(): empty stack");
    } else {
        T elem = elements.back();
        elements.pop_back();
        return elem;
    }
}

int main() {
    Queue<int> intQueue;
    intQueue.push(1);
    intQueue.push(2);
    std::cout << intQueue.pop() << "\n";
    std::cout << intQueue.pop() << "\n";

    Queue<std::string> strQueue;
    strQueue.push("Donald");
    strQueue.push("Goofy");
    std::cout << strQueue.pop() << "\n";
    std::cout << strQueue.pop() << "\n";

    return 0;
}
