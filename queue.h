#ifndef __QUEUE_H_INCLUDED__
#define __QUEUE_H_INCLUDED__

#include <string>
#include <vector>

template <typename T>
class Queue {
 private:
    std::vector<T> elements;
 public:
    void push(T);
    T pop();
};

template class Queue<int>;
template class Queue<std::string>;

#endif
