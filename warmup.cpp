#include <iostream>
#include <string>
#include <sstream>

int main() {
    int grade;
    double sum = 0;
    int n = 0;
    std::string input;

    for (int i = 0; i < 5; ++i) {
        while (true)  {
            std::cout << "Enter a grade: ";
            std::cin >> input;
            std::stringstream inputStream(input);
            if (inputStream >> grade) {
                sum += grade;
                n++;
                break;
            }
            std::cout << "The grade must be an integer\n";
        }
    }
    std::cout << "Mean: " << (sum / n) << "\n";

    return 0;
}
