#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>
#include "warmup2.h"

namespace students {

    double calculateMedian(std::vector<int> grades) {
        std::size_t size = grades.size();
        std::sort(grades.begin(), grades.end());
        if (size % 2 == 0) {
            return (grades[size / 2 - 1] + grades[size / 2]) / 2.0;
        } else {
            return grades[size / 2];
        }
    }
}

int main () {
    int grade;
    double sum = 0;
    int n = 0;
    std::vector<int> grades;
    std::vector<students::GradeRecord> gradeRecords;
    std::string firstNameInput;
    std::string lastNameInput;
    std::string gradeInput;

    while(true) {
        std::cout << "Enter the student's first name:\n";
        std::getline(std::cin, firstNameInput);
        if (firstNameInput == "") {
            break;
        }
        std::cout << "Enter the student's last name:\n";
        std::getline(std::cin, lastNameInput);
        while (true)  {
            std::cout << "Enter the student's grade:\n";
            std::getline(std::cin, gradeInput);
            std::stringstream inputStream(gradeInput);
            if (inputStream >> grade && grade >= 1 && grade <= 5) {
                break;
            }
            std::cout << "A grade must be an integer between 1 and 5, please try again\n";
        }
        sum += grade;
        n += 1;
        grades.push_back(grade);
        gradeRecords.push_back({ .firstName = firstNameInput,
                    .lastName = lastNameInput,
                    .grade = grade });
    }

    for (auto record: gradeRecords) {
        std::cout << record.lastName << " " << record.firstName << ": " << record.grade << "\n";
    }
    if (n >= 1) {
        std::cout << "The average of student grades is " << sum / n << "\n";
        std::cout << "The median of student grades is " << students::calculateMedian(grades) << "\n";
    }
    return 0;
}
