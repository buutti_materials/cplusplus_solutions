#ifndef __STUDENT_H_INCLUDED__
#define __STUDENT_H_INCLUDED__

#include <string>
#include <vector>

namespace students {

    struct GradeRecord {
        std::string firstName;
        std::string lastName;
        int grade;
    };

    double calculateMedian(const std::vector<double> grades);
}

#endif
